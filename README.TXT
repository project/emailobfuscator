Installation Instructions:

1. Copy the email_address_obfuscator module directory in your modules directory.

2. Enable email_address_obfuscator module at: Administer > Site building > Modules
