<?php
function emailobfuscator_admin_settings() {
  $options= array ();

  foreach (module_implements("email_obfuscator_engines") as $module) {
    $data= db_fetch_array(db_query("select info from {system} where name='%s'", $module));
    $data= unserialize($data['info']);
    $result= module_invoke($module, "email_obfuscator_engines");
    foreach ($result as $key => $value) {
      $tmp[$module.'::'.$key]= $value;
    }
    $options[$data['name']]= $tmp;
  }
  $form['emailobfuscator_engine']= array (
    '#type' => 'select',
    '#title' => t("Engines"),
    '#options' => $options,
    '#default_value' => emailobfuscator_get_engine()
  );
  return system_settings_form($form);
}